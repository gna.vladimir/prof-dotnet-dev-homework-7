﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        private static bool _exit = true;
        private static Regex _rgx = new(@"\d");
        static async Task Main(string[] args)
        {
            try
            {

                do
                {
                    await GetAllCustomersAsync();
                    await Console.Out.WriteLineAsync($"Введите id клиента для вывода подробной информации о клиенте\nВведите\"n\" для создания нового клиента\nВведите\"q\" для выхода");
                    string input = Console.ReadLine();
                    if (_rgx.IsMatch(input))
                    {
                        await GetCustomerByIdAsync(long.Parse(input));
                    }
                    else if (input.Equals("n", StringComparison.OrdinalIgnoreCase))
                    {
                        await CreateCustomerAsync();
                    }
                    else if (input.Equals("q", StringComparison.OrdinalIgnoreCase))
                    {
                        _exit = false;
                    }
                    else
                    {
                        await WriteDefectString();
                    }
                } while (_exit);
            }
            catch (Exception ex)
            {

                await Console.Out.WriteLineAsync(ex.Message);
            }

        }

        private static async Task CreateCustomerAsync()
        {
            do
            {
                await Console.Out.WriteLineAsync("Введите id клиента для создания нового пользователя\nВведите\"q\" для выхода");
                string input = Console.ReadLine();
                if (_rgx.IsMatch(input))
                {
                    var idCustomer = await CreateCustomerApiAsync(long.Parse(input), RandomCustomer());
                    if (idCustomer != -1)
                    {
                        await Console.Out.WriteAsync("Добавлен ");
                        await GetCustomerByIdAsync(idCustomer);
                    }
                }
                else if (input.Equals("q", StringComparison.OrdinalIgnoreCase))
                {
                    break;
                }
                else
                {
                    await WriteDefectString();
                }
            } while (true);
        }

        private static async Task<long> CreateCustomerApiAsync(long id, CustomerCreateRequest customerCreate)
        {
            var customer = new Customer() { Id = id, Firstname = customerCreate.Firstname, Lastname = customerCreate.Lastname };

            var content = await WebServiceApi.CreateAsync("customers", customer);
            if (content.Item1 != HttpStatusCode.OK)
            {
                await Console.Out.WriteLineAsync($"Статус код: {content.Item1} Сообщение: {content.Item2}");
                return -1;
            }
            return JsonConvert.DeserializeObject<long>(content.Item2);
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            return new CustomerCreateRequest();
        }

        private static async Task GetAllCustomersAsync()
        {
            await Console.Out.WriteLineAsync("Существующие Id клиентов:");
            IEnumerable<Customer> customers = JsonConvert.DeserializeObject<IEnumerable<Customer>>((await WebServiceApi.GetContentFromApiAsync("customers")).Item2);
            foreach (var item in customers)
            {
                await Console.Out.WriteLineAsync($"{item.Id}");
            }
        }

        private static async Task GetCustomerByIdAsync(long id)
        {
            var content = await WebServiceApi.GetContentFromApiAsync($"customers/{id}");
            if (content.Item1 != HttpStatusCode.OK)
            {
                await Console.Out.WriteLineAsync($"Статус код: {content.Item1} Сообщение: {content.Item2}");
            }
            else
            {
                Customer customer = JsonConvert.DeserializeObject<Customer>(content.Item2);
                await Console.Out.WriteLineAsync($"Пользователь с id: {id}");
                foreach (var prop in typeof(Customer).GetProperties())
                {
                    await Console.Out.WriteLineAsync($"\t{prop.Name} - {prop.GetValue(customer)}");
                }
            }
        }

        private static async Task WriteDefectString()
        {
            await Console.Out.WriteLineAsync("Введен не корректный символ!");
        }
    }
}