﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    public static class RandomExtensions
    {
        private static readonly Random _rnd = new();

        public static string String(this Random value, int size)
        {
            const char OFFSET = 'A';
            const int LETTEROFSET = 26;
            var sb = new StringBuilder();

            for (int i = 0; i < size; i++)
            {
                var @char = (char)_rnd.Next(OFFSET, OFFSET + LETTEROFSET);
                sb.Append(@char);
            }

            return sb.ToString();
        }
    }
}
