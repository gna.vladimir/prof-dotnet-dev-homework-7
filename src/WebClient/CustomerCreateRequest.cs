using System;

namespace WebClient
{
    public class CustomerCreateRequest
    {
        const int LENGTHNAME = 5;
        public CustomerCreateRequest()
        {
            Firstname =  new Random().String(LENGTHNAME);
            Lastname = new Random().String(LENGTHNAME);
        }

        public CustomerCreateRequest(
            string firstName,
            string lastName)
        {
            Firstname = firstName;
            Lastname = lastName;
        }

        public string Firstname { get; init; }

        public string Lastname { get; init; }
    }
}