﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    internal static class WebServiceApi
    {
        private static readonly string _urlApi;

        static WebServiceApi()
        {
            _urlApi = "https://localhost:5001/";
        }

        public static async Task<(HttpStatusCode, string)> GetContentFromApiAsync(string url)
        {
            HttpClient http = new();
            HttpResponseMessage response = await http.GetAsync(_urlApi + url);
            return (response.StatusCode, await response.Content.ReadAsStringAsync());
        }

        public static async Task<(HttpStatusCode, string)> CreateAsync(string url, object item)
        {
            HttpClient http = new();            
            HttpResponseMessage response = await http.PostAsJsonAsync(_urlApi + url, item);
            return (response.StatusCode, await response.Content.ReadAsStringAsync());
        }
    }
}
