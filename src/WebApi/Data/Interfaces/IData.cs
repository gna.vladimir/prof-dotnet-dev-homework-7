﻿using System.Collections.Generic;

namespace WebApi.Data.Interfaces
{
    public interface IData<T>
    {
        ICollection<T> Data { get; set; }
    }
}