﻿using System;
using System.Collections.Generic;
using WebApi.Data.Interfaces;
using WebApi.Models;

namespace WebApi.Data
{
    public class CustomerData : IData<Customer>
    {
        private ICollection<Customer> _customers;

        public ICollection<Customer> Data { get => _customers; set => _customers = value; }

        public CustomerData()
        {
            _customers = new List<Customer>()
            {
                new Customer() { Id = 1, Firstname = "Martin", Lastname = "Fauler" }
            };
        }
    }
}
