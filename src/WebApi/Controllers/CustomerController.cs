using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Data.Interfaces;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly IData<Customer> _customers;

        public CustomerController(IData<Customer> customers)
        {
            _customers = customers;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<ActionResult<IEnumerable<Customer>>> GetCustomersAsync()
        {
            return Ok(await Task.FromResult(_customers.Data.ToList()));
        }

        [HttpGet("{id:long}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] long id)
        {
            var existCustomer = await Task.FromResult(_customers.Data.FirstOrDefault(c => c.Id == id));
            if (existCustomer is null)
            {
                return NotFound($"Not found customer with id = {id}");
            }
            return Ok(existCustomer);
        }

        [HttpPost("")]
        [ProducesResponseType(202)]
        [ProducesResponseType(409)]
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customer)
        {
            if (_customers.Data.Where(c => c.Id == customer.Id).Any())
            {
                return Conflict($"Customer with id {customer.Id} exists");
            }
            await Task.Run(() => _customers.Data.Add(customer));
            return Ok(customer.Id);
        }
    }
}